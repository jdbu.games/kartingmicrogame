using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KartGame.KartSystems;
using UnityEngine.Events;

public class SlideController : MonoBehaviour
{
    [SerializeField]
    private float turnForce;
    [SerializeField]
    private new Rigidbody rigidbody;
    [SerializeField]
    private bool sliding;
    [SerializeField]
    private SpeedometerController speedometerController;
    [SerializeField]
    private ArcadeKart arcadeKart;
    [SerializeField]
    private WarningController warningController;
    [SerializeField]
    private ObjectiveReachTargets objectiveReachTargets;
    [SerializeField]
    private UnityEvent onSliding;


    // Update is called once per frame
    void FixedUpdate()
    {
        if (sliding == true)
        {
            Slide();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //print(other.name);
        if (other.tag == "Puddle")
        {
            if(sliding == false && speedometerController.GetSpeed() > 40 && objectiveReachTargets.GetIsCompleted() == false)
            {
                sliding = true;

                onSliding?.Invoke();

                Invoke(nameof(StopSliding), 2f);
                Invoke(nameof(warningController.EnableWarningMessage), 3f);
            }
        }
    }

    private void Slide()
    {
        rigidbody.AddTorque(transform.up * turnForce * 50);
    }

    private void StopSliding()
    {
        sliding = false;
        arcadeKart.m_CanMove = false;
    }

    private void EnableWarningMessage()
    {
        warningController.EnableWarningMessage();
    }
}
