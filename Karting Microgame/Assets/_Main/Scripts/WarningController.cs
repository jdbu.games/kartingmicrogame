using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningController : MonoBehaviour
{
    [SerializeField]
    private DisplayMessage displayMessage;
    [SerializeField]
    private LoadSceneController loadSceneController;

    public void EnableWarningMessage()
    {
        displayMessage.message = "Has sufrido un accidente por deslizamiento.";
        displayMessage.Display();
        Invoke(nameof(LoadLoseScene), 4f);
    }

    public void LoadLoseScene()
    {
        loadSceneController.LoadLoseScene();
    }
}
