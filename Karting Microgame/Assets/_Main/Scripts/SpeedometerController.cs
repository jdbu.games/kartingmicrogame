using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpeedometerController : MonoBehaviour
{
    [SerializeField]
    private float speed;
    [SerializeField]
    private bool isPlaying;
    [SerializeField]
    private TextMeshProUGUI speedText;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CheckSpeed());
    }

    IEnumerator CheckSpeed()
    {
        while (isPlaying)
        {
            Vector3 previusPosition = transform.position;

            yield return new WaitForFixedUpdate();

            speed = (Mathf.RoundToInt(Vector3.Distance(transform.position, previusPosition) / Time.fixedDeltaTime)) * 1.25f;

            speedText.text = speed.ToString("0");
        }
    }

    public int GetSpeed()
    {
        return (int)speed;
    }
}
